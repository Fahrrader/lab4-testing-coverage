package DAO;

import com.hw.db.DAO.ThreadDAO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

public class ThreadDAOTests {
    private final int mockId = 1;
    private final int mockSince = (int) System.currentTimeMillis();
    private final int mockLimit = 42;
    private final JdbcTemplate mockJdbcTemplate = Mockito.mock(JdbcTemplate.class);
    private final ThreadDAO mockThreadDAO = new ThreadDAO(mockJdbcTemplate);

    @Test
    void treeSortSuccess1() {
        Assertions.assertEquals(List.of(), ThreadDAO.treeSort(mockId, mockLimit, mockSince, false));
    }

    @Test
    void treeSortSuccess2() {
        Assertions.assertEquals(List.of(), ThreadDAO.treeSort(mockId, mockLimit, mockSince, true));
    }
}
