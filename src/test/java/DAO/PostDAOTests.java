package DAO;

import com.hw.db.DAO.PostDAO;
import com.hw.db.models.Post;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.Timestamp;
import java.util.Date;

public class PostDAOTests {
    private JdbcTemplate mockJdbcTemplate;
    private PostDAO mockPostDAO;
    private final int mockId = 5;
    private final Timestamp mockCreated = new Timestamp(new Date().getTime());
    private final String mockAuthor = "author";
    private final String mockForum = "forum";
    private final String mockMessage = "message";
    private final int mockParent = 1;
    private final int mockThread = 2;


    @BeforeEach
    @DisplayName("User DAO tests set up")
    void beforePostTests() {
        mockJdbcTemplate = Mockito.mock(JdbcTemplate.class);
        mockPostDAO = new PostDAO(mockJdbcTemplate);

        Mockito.when(mockJdbcTemplate.queryForObject(
                Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.eq(mockId)))
                .thenReturn(new Post("anotherAuthor", new Timestamp(0), mockForum, "anotherMessage", 3, mockThread, true));
    }

    @Test
    void setPostSuccess() {
        PostDAO.setPost(mockId, new Post(null, null, mockForum, null, mockParent, mockThread, true));
        Mockito.verify(mockJdbcTemplate).queryForObject(
                Mockito.eq("SELECT * FROM \"posts\" WHERE id=? LIMIT 1;"),
                Mockito.any(PostDAO.PostMapper.class),
                Mockito.eq(mockId));
    }

    @Test
    void setPostSuccess2() {
        PostDAO.setPost(mockId, new Post(mockAuthor, null, mockForum, null, mockParent, mockThread, true));
        Mockito.verify(mockJdbcTemplate).update(
                Mockito.eq("UPDATE \"posts\" SET  author=?  , isEdited=true WHERE id=?;"),
                Mockito.eq(mockAuthor),
                Mockito.eq(mockId)
        );
    }

    @Test
    void setPostSuccess3() {
        PostDAO.setPost(mockId, new Post(null, mockCreated, mockForum, null, mockParent, mockThread, true));
        Mockito.verify(mockJdbcTemplate).update(
                Mockito.eq("UPDATE \"posts\" SET  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Mockito.eq(mockCreated),
                Mockito.eq(mockId)
        );
    }

    @Test
    void setPostSuccess4() {
        PostDAO.setPost(mockId, new Post(null, null, mockForum, mockMessage, mockParent, mockThread, true));
        Mockito.verify(mockJdbcTemplate).update(
                Mockito.eq("UPDATE \"posts\" SET  message=?  , isEdited=true WHERE id=?;"),
                Mockito.eq(mockMessage),
                Mockito.eq(mockId)
        );
    }

    @Test
    void setPostSuccess5() {
        PostDAO.setPost(mockId, new Post(mockAuthor, mockCreated, mockForum, null, mockParent, mockThread, true));
        Mockito.verify(mockJdbcTemplate).update(
                Mockito.eq("UPDATE \"posts\" SET  author=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Mockito.eq(mockAuthor),
                Mockito.eq(mockCreated),
                Mockito.eq(mockId)
        );
    }

    @Test
    void setPostSuccess6() {
        PostDAO.setPost(mockId, new Post(mockAuthor, null, mockForum, mockMessage, mockParent, mockThread, true));
        Mockito.verify(mockJdbcTemplate).update(
                Mockito.eq("UPDATE \"posts\" SET  author=?  ,  message=?  , isEdited=true WHERE id=?;"),
                Mockito.eq(mockAuthor),
                Mockito.eq(mockMessage),
                Mockito.eq(mockId)
        );
    }

    @Test
    void setPostSuccess7() {
        PostDAO.setPost(mockId, new Post(null, mockCreated, mockForum, mockMessage, mockParent, mockThread, true));
        Mockito.verify(mockJdbcTemplate).update(
                Mockito.eq("UPDATE \"posts\" SET  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Mockito.eq(mockMessage),
                Mockito.eq(mockCreated),
                Mockito.eq(mockId)
        );
    }


    @Test
    void setPostSuccess8() {
        PostDAO.setPost(mockId, new Post(mockAuthor, mockCreated, mockForum, mockMessage, mockParent, mockThread, true));
        Mockito.verify(mockJdbcTemplate).update(
                Mockito.eq("UPDATE \"posts\" SET  author=?  ,  message=?  ,  created=(?)::TIMESTAMPTZ  , isEdited=true WHERE id=?;"),
                Mockito.eq(mockAuthor),
                Mockito.eq(mockMessage),
                Mockito.eq(mockCreated),
                Mockito.eq(mockId)
        );
    }
}
