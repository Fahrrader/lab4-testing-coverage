package DAO;

import com.hw.db.DAO.ForumDAO;
import com.hw.db.models.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.List;

public class ForumDAOTests {
    private final String mockSince = String.valueOf(System.currentTimeMillis());

    private final int mockLimit = 10;

    private final String mockSlug = "slug";

    @Test
    void userListSuccess() {
        JdbcTemplate jdbc = Mockito.mock(JdbcTemplate.class);
        ForumDAO dao = new ForumDAO(jdbc);

        List<User> result = ForumDAO.UserList(mockSlug, mockLimit, mockSince, true);
        Assertions.assertEquals(List.of(), result);
    }

    @Test
    void userListSuccessDescNull() {
        JdbcTemplate jdbc = Mockito.mock(JdbcTemplate.class);
        ForumDAO dao = new ForumDAO(jdbc);

        List<User> result = ForumDAO.UserList(mockSlug, mockLimit, mockSince, null);
        Assertions.assertEquals(List.of(), result);
    }

    @Test
    void userListSuccessLimitSinceNullDescFalse() {
        JdbcTemplate jdbc = Mockito.mock(JdbcTemplate.class);
        ForumDAO dao = new ForumDAO(jdbc);

        List<User> result = ForumDAO.UserList(mockSlug, null, null, false);
        Assertions.assertEquals(List.of(), result);
    }
}
